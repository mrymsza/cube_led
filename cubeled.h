#ifndef CUBELED_H
#define CUBELED_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QMessageBox>

namespace Ui {
class CUBELed;
}

class CUBELed : public QMainWindow
{
    Q_OBJECT

public:
    explicit CUBELed(QWidget *parent = 0);
    ~CUBELed();


private slots:


    int dec_to_bin(int liczba);

    void on_sendButton_clicked();

    void writeData(const QByteArray &data);

    void readData();

    void on_conectButton_clicked();

    void on_dial_3_valueChanged(int value);


    void on_pushButton_3_clicked();


    void on_dtLed_dial_valueChanged(int value);

    void on_pushButton_2_clicked();

private:
    unsigned int calculateLayer(int n);
    QByteArray getLayerData();
    QByteArray convertL(unsigned int LV);
    QVector<unsigned char> intToBytes(unsigned int paramInt);

private:
    Ui::CUBELed *ui;
    QSerialPort *serial;
};

#endif // CUBELED_H
