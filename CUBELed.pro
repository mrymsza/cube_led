#-------------------------------------------------
#
# Project created by QtCreator 2016-12-09T14:09:36
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CUBELed
TEMPLATE = app


SOURCES += main.cpp\
        cubeled.cpp

HEADERS  += cubeled.h

FORMS    += cubeled.ui

DESTDIR = builds
OBJECTS_DIR = builds/objects
MOC_DIR = builds/mocs
