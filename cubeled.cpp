#include "cubeled.h"
#include "ui_cubeled.h"
#include <QDebug>
#include <vector>


CUBELed::CUBELed(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CUBELed)
{
    ui->setupUi(this);

    //zainicjowanie obiektu;
    serial = new QSerialPort(this);
    for(int i=0; i<10; i++)
    {
        ui->comboBox->addItem("COM" + QString::number(i+1));
    }
    //podłączenie slotów;
    connect(serial, SIGNAL(error(QSerialPort::SerialPortError)), this,
            SLOT(handleError(QSerialPort::SerialPortError)));
    connect(serial, SIGNAL(readyRead()), this, SLOT(readData()));

}

CUBELed::~CUBELed()
{
      delete ui;
}

unsigned int CUBELed::calculateLayer(int n)
{
    unsigned int result=0;
    switch (n) {
    case 0:

        if(ui->LED11->isChecked())
        {
            result+=1;
        }

        if(ui->LED12->isChecked())
        {
            result+=2;
        }
        if(ui->LED14->isChecked())
        {
            result+=4;
        }

        if(ui->LED18->isChecked())
        {
            result+=8;
        }
        if(ui->LED116->isChecked())
        {
            result+=16;
        }
        if(ui->LED132->isChecked())
        {
            result+=32;
        }
        if(ui->LED164->isChecked())
        {
            result+=64;
        }
        if(ui->LED1128->isChecked())
        {
            result+=128;
        }
        if(ui->LED1256->isChecked())
        {
            result+=256;
        }
        break;
    case 1:
        if(ui->LED21->isChecked())
        {
            result+=1;
        }

        if(ui->LED22->isChecked())
        {
            result+=2;
        }
        if(ui->LED24->isChecked())
        {
            result+=4;
        }

        if(ui->LED28->isChecked())
        {
            result+=8;
        }
        if(ui->LED216->isChecked())
        {
            result+=16;
        }

        if(ui->LED232->isChecked())
        {
            result+=32;
        }
        if(ui->LED264->isChecked())
        {
            result+=64;
        }

        if(ui->LED2128->isChecked())
        {
            result+=128;
        }
        if(ui->LED2256->isChecked())
        {
            result+=256;
        }
        break;
    case 2:
        if(ui->LED31->isChecked())
        {
            result+=1;
        }

        if(ui->LED32->isChecked())
        {
            result+=2;
        }
        if(ui->LED34->isChecked())
        {
            result+=4;
        }

        if(ui->LED38->isChecked())
        {
            result+=8;
        }
        if(ui->LED316->isChecked())
        {
            result+=16;
        }

        if(ui->LED332->isChecked())
        {
            result+=32;
        }
        if(ui->LED364->isChecked())
        {
            result+=64;
        }

        if(ui->LED3128->isChecked())
        {
            result+=128;
        }
        if(ui->LED3256->isChecked())
        {
            result+=256;
        }
        break;
    default:
        break;
    }
    return result;
}

QByteArray CUBELed::getLayerData()
{
    QByteArray msg;
    msg.append("#");
//    msg.append("z");
    if(ui->dial_3->value()>255)
    {
       msg.append((char)(255));
       qDebug()<<"tutaj jestem";
    }
    else{
        msg.append((char)(ui->dial_3->value()));

    }

    unsigned int L0 = calculateLayer(0);
    unsigned int L1 = calculateLayer(1);
    unsigned int L2 = calculateLayer(2);
    msg.append(convertL(L0));
    msg.append(convertL(L1));
    msg.append(convertL(L2));
    msg.append((char)(ui->dtLed->value()));
    msg.append("\n");
    return msg;
}
QVector<unsigned char> CUBELed::intToBytes(unsigned int paramInt)
{
     QVector<unsigned char> arrayOfByte(4);
     for (int i = 0; i < 4; i++)
         arrayOfByte[3 - i] = (paramInt >> (i * 8));
     return arrayOfByte;
}
QByteArray CUBELed::convertL(unsigned int LV)
{
    //QString tmp = QString::number(LV>>8);
    QVector<unsigned char> tmp2 = intToBytes(LV);

    QByteArray tmp;//= QByteArray(tmp2.at(2));
    tmp.append(tmp2.at(2));
    tmp.append(tmp2.at(3));
//    qDebug()<<(unsigned char)tmp.at(2);
//    qDebug()<<(unsigned char)tmp.at(3);

    return tmp;
}


void CUBELed::on_sendButton_clicked()
{


    //connect arduino
    QByteArray msg = getLayerData();


//    QByteArray msg2;
//    msg2.resize(8);
//    send
    /*for(int i=0; i<msg.length(); i++)
    {
        msg2.insert(i,msg.toUtf8().at(i));
//        msg2[i]=msg.at(i);
        qDebug()<<(int)msg2[i];
        QByteArray abc()
    }
    writeData(msg2);*/

//    QByteArray array (msg.toStdString().c_str());
    writeData(msg);

}

void CUBELed::writeData(const QByteArray &data)
{
    qDebug()<<"do arduino: ["<<data<<"]";
    serial->write(data);

}

void CUBELed::readData()
{
    if(serial->canReadLine()){
        QByteArray data = serial->readAll();
        qDebug()<<data;


    }else{
        //serial->clear();
    }
}

void CUBELed::on_conectButton_clicked()
{
    //podstawowa konfiguracja i uruchomienie portu

    serial->setPortName(ui->comboBox->currentText());
    serial->setBaudRate(9600);
    serial->setDataBits(static_cast<QSerialPort::DataBits>(8));
    serial->setParity(static_cast<QSerialPort::Parity>(0));
    serial->setStopBits(static_cast<QSerialPort::StopBits>(1));
    serial->setFlowControl(static_cast<QSerialPort::FlowControl>(0));
    if(serial->open(QIODevice::ReadWrite)){
        ui->connectStatus->setText("połączono");
        writeData("?\r\n");
} else {
        ui->connectStatus->setText("nie połączono");

QMessageBox::critical(this, tr("Error"), serial->errorString());
    }
}

void CUBELed::on_dial_3_valueChanged(int value)
{
    ui->dtLayer->display(value);
}

void CUBELed::on_pushButton_3_clicked()
{
    writeData("?\r\n");
}


void CUBELed::on_dtLed_dial_valueChanged(int value)
{
    ui->dtLed->display(value);
}
