/****************************************************************************
** Meta object code from reading C++ file 'cubeled.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../cubeled.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cubeled.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_CUBELed_t {
    QByteArrayData data[14];
    char stringdata0[201];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CUBELed_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CUBELed_t qt_meta_stringdata_CUBELed = {
    {
QT_MOC_LITERAL(0, 0, 7), // "CUBELed"
QT_MOC_LITERAL(1, 8, 10), // "dec_to_bin"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 6), // "liczba"
QT_MOC_LITERAL(4, 27, 21), // "on_sendButton_clicked"
QT_MOC_LITERAL(5, 49, 9), // "writeData"
QT_MOC_LITERAL(6, 59, 4), // "data"
QT_MOC_LITERAL(7, 64, 8), // "readData"
QT_MOC_LITERAL(8, 73, 23), // "on_conectButton_clicked"
QT_MOC_LITERAL(9, 97, 22), // "on_dial_3_valueChanged"
QT_MOC_LITERAL(10, 120, 5), // "value"
QT_MOC_LITERAL(11, 126, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(12, 150, 26), // "on_dtLed_dial_valueChanged"
QT_MOC_LITERAL(13, 177, 23) // "on_pushButton_2_clicked"

    },
    "CUBELed\0dec_to_bin\0\0liczba\0"
    "on_sendButton_clicked\0writeData\0data\0"
    "readData\0on_conectButton_clicked\0"
    "on_dial_3_valueChanged\0value\0"
    "on_pushButton_3_clicked\0"
    "on_dtLed_dial_valueChanged\0"
    "on_pushButton_2_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CUBELed[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x08 /* Private */,
       4,    0,   62,    2, 0x08 /* Private */,
       5,    1,   63,    2, 0x08 /* Private */,
       7,    0,   66,    2, 0x08 /* Private */,
       8,    0,   67,    2, 0x08 /* Private */,
       9,    1,   68,    2, 0x08 /* Private */,
      11,    0,   71,    2, 0x08 /* Private */,
      12,    1,   72,    2, 0x08 /* Private */,
      13,    0,   75,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Int, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,

       0        // eod
};

void CUBELed::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CUBELed *_t = static_cast<CUBELed *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        //case 0: { int _r = _t->dec_to_bin((*reinterpret_cast< int(*)>(_a[1])));
          //  if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 1: _t->on_sendButton_clicked(); break;
        case 2: _t->writeData((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->readData(); break;
        case 4: _t->on_conectButton_clicked(); break;
        case 5: _t->on_dial_3_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_pushButton_3_clicked(); break;
        case 7: _t->on_dtLed_dial_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        //case 8: _t->on_pushButton_2_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject CUBELed::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_CUBELed.data,
      qt_meta_data_CUBELed,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CUBELed::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CUBELed::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CUBELed.stringdata0))
        return static_cast<void*>(const_cast< CUBELed*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int CUBELed::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
