/********************************************************************************
** Form generated from reading UI file 'cubeled.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CUBELED_H
#define UI_CUBELED_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDial>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CUBELed
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QGridLayout *gridLayout_7;
    QCheckBox *LED132;
    QCheckBox *LED116;
    QCheckBox *LED12;
    QCheckBox *LED14;
    QCheckBox *LED1256;
    QCheckBox *LED11;
    QCheckBox *LED18;
    QCheckBox *LED164;
    QCheckBox *LED1128;
    QFrame *line_2;
    QGridLayout *gridLayout_8;
    QCheckBox *LED232;
    QCheckBox *LED22;
    QCheckBox *LED216;
    QCheckBox *LED21;
    QCheckBox *LED24;
    QCheckBox *LED28;
    QCheckBox *LED264;
    QCheckBox *LED2128;
    QCheckBox *LED2256;
    QFrame *line_3;
    QGridLayout *gridLayout_9;
    QCheckBox *LED31;
    QCheckBox *LED32;
    QCheckBox *LED34;
    QCheckBox *LED332;
    QCheckBox *LED316;
    QCheckBox *LED38;
    QCheckBox *LED364;
    QCheckBox *LED3128;
    QCheckBox *LED3256;
    QFrame *line_4;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_5;
    QLabel *label;
    QDial *dial_3;
    QLCDNumber *dtLayer;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_4;
    QDial *dtLed_dial;
    QLCDNumber *dtLed;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_2;
    QDial *dial_2;
    QLCDNumber *lcdNumber;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_3;
    QDial *dial;
    QLCDNumber *lcdNumber_2;
    QFrame *line_5;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label_5;
    QComboBox *comboBox;
    QPushButton *conectButton;
    QLabel *connectStatus;
    QFrame *line;
    QPushButton *pushButton_3;
    QPushButton *pushButton_2;
    QPushButton *sendButton;

    void setupUi(QMainWindow *CUBELed)
    {
        if (CUBELed->objectName().isEmpty())
            CUBELed->setObjectName(QStringLiteral("CUBELed"));
        CUBELed->resize(607, 398);
        centralWidget = new QWidget(CUBELed);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_4 = new QVBoxLayout(centralWidget);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, -1, 0, -1);
        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        gridLayout_7->setContentsMargins(-1, -1, 10, -1);
        LED132 = new QCheckBox(centralWidget);
        LED132->setObjectName(QStringLiteral("LED132"));

        gridLayout_7->addWidget(LED132, 2, 1, 1, 1);

        LED116 = new QCheckBox(centralWidget);
        LED116->setObjectName(QStringLiteral("LED116"));

        gridLayout_7->addWidget(LED116, 1, 1, 1, 1);

        LED12 = new QCheckBox(centralWidget);
        LED12->setObjectName(QStringLiteral("LED12"));

        gridLayout_7->addWidget(LED12, 1, 0, 1, 1);

        LED14 = new QCheckBox(centralWidget);
        LED14->setObjectName(QStringLiteral("LED14"));

        gridLayout_7->addWidget(LED14, 2, 0, 1, 1);

        LED1256 = new QCheckBox(centralWidget);
        LED1256->setObjectName(QStringLiteral("LED1256"));

        gridLayout_7->addWidget(LED1256, 2, 2, 1, 1);

        LED11 = new QCheckBox(centralWidget);
        LED11->setObjectName(QStringLiteral("LED11"));

        gridLayout_7->addWidget(LED11, 0, 0, 1, 1);

        LED18 = new QCheckBox(centralWidget);
        LED18->setObjectName(QStringLiteral("LED18"));

        gridLayout_7->addWidget(LED18, 0, 1, 1, 1);

        LED164 = new QCheckBox(centralWidget);
        LED164->setObjectName(QStringLiteral("LED164"));

        gridLayout_7->addWidget(LED164, 0, 2, 1, 1);

        LED1128 = new QCheckBox(centralWidget);
        LED1128->setObjectName(QStringLiteral("LED1128"));

        gridLayout_7->addWidget(LED1128, 1, 2, 1, 1);


        horizontalLayout_3->addLayout(gridLayout_7);

        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        horizontalLayout_3->addWidget(line_2);

        gridLayout_8 = new QGridLayout();
        gridLayout_8->setSpacing(6);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        LED232 = new QCheckBox(centralWidget);
        LED232->setObjectName(QStringLiteral("LED232"));

        gridLayout_8->addWidget(LED232, 2, 1, 1, 1);

        LED22 = new QCheckBox(centralWidget);
        LED22->setObjectName(QStringLiteral("LED22"));

        gridLayout_8->addWidget(LED22, 1, 0, 1, 1);

        LED216 = new QCheckBox(centralWidget);
        LED216->setObjectName(QStringLiteral("LED216"));

        gridLayout_8->addWidget(LED216, 1, 1, 1, 1);

        LED21 = new QCheckBox(centralWidget);
        LED21->setObjectName(QStringLiteral("LED21"));

        gridLayout_8->addWidget(LED21, 0, 0, 1, 1);

        LED24 = new QCheckBox(centralWidget);
        LED24->setObjectName(QStringLiteral("LED24"));

        gridLayout_8->addWidget(LED24, 2, 0, 1, 1);

        LED28 = new QCheckBox(centralWidget);
        LED28->setObjectName(QStringLiteral("LED28"));

        gridLayout_8->addWidget(LED28, 0, 1, 1, 1);

        LED264 = new QCheckBox(centralWidget);
        LED264->setObjectName(QStringLiteral("LED264"));

        gridLayout_8->addWidget(LED264, 0, 2, 1, 1);

        LED2128 = new QCheckBox(centralWidget);
        LED2128->setObjectName(QStringLiteral("LED2128"));

        gridLayout_8->addWidget(LED2128, 1, 2, 1, 1);

        LED2256 = new QCheckBox(centralWidget);
        LED2256->setObjectName(QStringLiteral("LED2256"));

        gridLayout_8->addWidget(LED2256, 2, 2, 1, 1);


        horizontalLayout_3->addLayout(gridLayout_8);

        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);

        horizontalLayout_3->addWidget(line_3);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        LED31 = new QCheckBox(centralWidget);
        LED31->setObjectName(QStringLiteral("LED31"));

        gridLayout_9->addWidget(LED31, 0, 0, 1, 1);

        LED32 = new QCheckBox(centralWidget);
        LED32->setObjectName(QStringLiteral("LED32"));

        gridLayout_9->addWidget(LED32, 1, 0, 1, 1);

        LED34 = new QCheckBox(centralWidget);
        LED34->setObjectName(QStringLiteral("LED34"));

        gridLayout_9->addWidget(LED34, 2, 0, 1, 1);

        LED332 = new QCheckBox(centralWidget);
        LED332->setObjectName(QStringLiteral("LED332"));

        gridLayout_9->addWidget(LED332, 2, 1, 1, 1);

        LED316 = new QCheckBox(centralWidget);
        LED316->setObjectName(QStringLiteral("LED316"));

        gridLayout_9->addWidget(LED316, 1, 1, 1, 1);

        LED38 = new QCheckBox(centralWidget);
        LED38->setObjectName(QStringLiteral("LED38"));

        gridLayout_9->addWidget(LED38, 0, 1, 1, 1);

        LED364 = new QCheckBox(centralWidget);
        LED364->setObjectName(QStringLiteral("LED364"));

        gridLayout_9->addWidget(LED364, 0, 2, 1, 1);

        LED3128 = new QCheckBox(centralWidget);
        LED3128->setObjectName(QStringLiteral("LED3128"));

        gridLayout_9->addWidget(LED3128, 1, 2, 1, 1);

        LED3256 = new QCheckBox(centralWidget);
        LED3256->setObjectName(QStringLiteral("LED3256"));

        gridLayout_9->addWidget(LED3256, 2, 2, 1, 1);


        horizontalLayout_3->addLayout(gridLayout_9);


        verticalLayout_4->addLayout(horizontalLayout_3);

        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);

        verticalLayout_4->addWidget(line_4);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_5->addWidget(label);

        dial_3 = new QDial(centralWidget);
        dial_3->setObjectName(QStringLiteral("dial_3"));
        dial_3->setMaximum(255);

        verticalLayout_5->addWidget(dial_3);

        dtLayer = new QLCDNumber(centralWidget);
        dtLayer->setObjectName(QStringLiteral("dtLayer"));

        verticalLayout_5->addWidget(dtLayer);


        horizontalLayout_4->addLayout(verticalLayout_5);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_2->addWidget(label_4);

        dtLed_dial = new QDial(centralWidget);
        dtLed_dial->setObjectName(QStringLiteral("dtLed_dial"));
        dtLed_dial->setMaximum(255);

        verticalLayout_2->addWidget(dtLed_dial);

        dtLed = new QLCDNumber(centralWidget);
        dtLed->setObjectName(QStringLiteral("dtLed"));

        verticalLayout_2->addWidget(dtLed);


        horizontalLayout_4->addLayout(verticalLayout_2);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_6->addWidget(label_2);

        dial_2 = new QDial(centralWidget);
        dial_2->setObjectName(QStringLiteral("dial_2"));

        verticalLayout_6->addWidget(dial_2);

        lcdNumber = new QLCDNumber(centralWidget);
        lcdNumber->setObjectName(QStringLiteral("lcdNumber"));

        verticalLayout_6->addWidget(lcdNumber);


        horizontalLayout_4->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_7->addWidget(label_3);

        dial = new QDial(centralWidget);
        dial->setObjectName(QStringLiteral("dial"));

        verticalLayout_7->addWidget(dial);

        lcdNumber_2 = new QLCDNumber(centralWidget);
        lcdNumber_2->setObjectName(QStringLiteral("lcdNumber_2"));

        verticalLayout_7->addWidget(lcdNumber_2);


        horizontalLayout_4->addLayout(verticalLayout_7);


        verticalLayout_4->addLayout(horizontalLayout_4);

        line_5 = new QFrame(centralWidget);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        verticalLayout_4->addWidget(line_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout->addWidget(label_5);

        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        verticalLayout->addWidget(comboBox);

        conectButton = new QPushButton(centralWidget);
        conectButton->setObjectName(QStringLiteral("conectButton"));

        verticalLayout->addWidget(conectButton);

        connectStatus = new QLabel(centralWidget);
        connectStatus->setObjectName(QStringLiteral("connectStatus"));

        verticalLayout->addWidget(connectStatus);


        horizontalLayout_2->addLayout(verticalLayout);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout_2->addWidget(line);

        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        horizontalLayout_2->addWidget(pushButton_3);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout_2->addWidget(pushButton_2);

        sendButton = new QPushButton(centralWidget);
        sendButton->setObjectName(QStringLiteral("sendButton"));

        horizontalLayout_2->addWidget(sendButton);


        verticalLayout_4->addLayout(horizontalLayout_2);

        CUBELed->setCentralWidget(centralWidget);

        retranslateUi(CUBELed);

        QMetaObject::connectSlotsByName(CUBELed);
    } // setupUi

    void retranslateUi(QMainWindow *CUBELed)
    {
        CUBELed->setWindowTitle(QApplication::translate("CUBELed", "CUBELed", 0));
        LED132->setText(QString());
        LED116->setText(QString());
        LED12->setText(QString());
        LED14->setText(QString());
        LED1256->setText(QString());
        LED11->setText(QString());
        LED18->setText(QString());
        LED164->setText(QString());
        LED1128->setText(QString());
        LED232->setText(QString());
        LED22->setText(QString());
        LED216->setText(QString());
        LED21->setText(QString());
        LED24->setText(QString());
        LED28->setText(QString());
        LED264->setText(QString());
        LED2128->setText(QString());
        LED2256->setText(QString());
        LED31->setText(QString());
        LED32->setText(QString());
        LED34->setText(QString());
        LED332->setText(QString());
        LED316->setText(QString());
        LED38->setText(QString());
        LED364->setText(QString());
        LED3128->setText(QString());
        LED3256->setText(QString());
        label->setText(QApplication::translate("CUBELed", "op\303\263\305\272nienie pomi\304\231dzy warstwami", 0));
        label_4->setText(QApplication::translate("CUBELed", "op\303\263\305\272nienie mi\304\231dzy ledami", 0));
        label_2->setText(QApplication::translate("CUBELed", "op\303\263\305\272nienie pomi\304\231dzy sekwencjami", 0));
        label_3->setText(QApplication::translate("CUBELed", "jasno\305\233\304\207 diod", 0));
        label_5->setText(QApplication::translate("CUBELed", "Port COM:", 0));
        conectButton->setText(QApplication::translate("CUBELed", "connect", 0));
        connectStatus->setText(QString());
        pushButton_3->setText(QApplication::translate("CUBELed", "Test", 0));
        pushButton_2->setText(QApplication::translate("CUBELed", "Next", 0));
        sendButton->setText(QApplication::translate("CUBELed", "Send", 0));
    } // retranslateUi

};

namespace Ui {
    class CUBELed: public Ui_CUBELed {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CUBELED_H
