//Digital
#define D0 0
#define D1 1
#define D2 2
#define D3 3
#define D4 4
#define D5 5
#define D6 6
#define D7 7
#define D8 8
#define D9 9
#define D10 10
#define D11 11
#define D12 12
#define D13 13

int columnPort[9];
int layerPort[3];

unsigned char dt = 1000/(27*32);
unsigned char dtLed = 1000/(27*32);
int onLed=0;
int onPort=0;
int onLayer=0;
int ledState[27];  //led

boolean CMD_STOP;
String CMD;

void setup() 
{
    Serial.begin(9600);
    Serial.println("Hello! I am cube led and I am turned on!");
    
    columnPort[0]=D2;
    columnPort[1]=D3;
    columnPort[2]=D4;
    columnPort[3]=D5;
    columnPort[4]=D6;
    columnPort[5]=D7;
    columnPort[6]=D8;
    columnPort[7]=D9;
    columnPort[8]=D10;

    layerPort[0]=D11;
    layerPort[1]=D12;
    layerPort[2]=D13;

    for(int i=0;i<9;i++)
      pinMode(columnPort[i], OUTPUT);

    for(int i=0;i<3;i++)
      pinMode(layerPort[i], OUTPUT);

    for(int i=0;i<27;i++)
      ledState[i]=1;
}

void runSerial()
{
  while (Serial.available())
  {
    char inChar = (char)Serial.read();
    CMD += inChar;
    if (inChar == '\n')
    {
      CMD_STOP = true;
    }
  }
}

void setLed(String cmd)
{
  dt = (unsigned char)CMD[1];
  
  ledState[0]=(unsigned char)(CMD[2])&0x01;
  ledState[9]=(unsigned char)(CMD[4])&0x01;
  ledState[18]=(unsigned char)(CMD[6])&0x01;
  
  int k=1;
  for(unsigned char i=0x80; i>=0x01; i=i>>1)
  {
    ledState[k]=((unsigned char)(CMD[3])&i)?(1):(0);
    ledState[9 + k]=((unsigned char)(CMD[5])&i)?(1):(0);
    ledState[18 + k]=((unsigned char)(CMD[7])&i)?(1):(0);
    k++;
  }
  dtLed=(unsigned char)CMD[8];
  for(int i=0;i<27;i++)
    Serial.print(ledState[i]);
  Serial.println("");
}

int modulo( int dividend, int divisor )
{
  int ret =  dividend % divisor;
  if(ret<0)
  {
    ret += divisor;
  }
  return ret;
}

void loop()
{
  runSerial();
  
  if(CMD_STOP)
  {
    switch(CMD[0])
    {
      case '?':
      {
        Serial.write("\nCubeLed is connected!\n");
      }
      break;
      case 'T':
      {
        dt = (int)CMD[1];
      }break;
      case'#':
      {
        Serial.print("Data in ardu:");
        for( int i=0; i<9; i++)
        {
          Serial.print((unsigned int)(unsigned char)(CMD[i]));
          
          Serial.print(", ");
          
        }
        Serial.println(" ");
        setLed(CMD);
      }
      break;
      default:
      {
        Serial.write("\nuknown cmd\n");
      }
      break;
    }
    CMD="";
    CMD_STOP = false;
  }

for(int i=0;i<9;i++){
  onPort=modulo(onLed,9);
  if(ledState[onLed]!=0)
    digitalWrite(columnPort[onPort], HIGH);
    else
    digitalWrite(columnPort[onPort], LOW);
  digitalWrite(layerPort[onLayer], HIGH);  

  if(++onLed==27)
  {
    onLed=0;
  }
  if((onPort==8) && (++onLayer==3))
  {
    onLayer=0;
  }
  delay(dtLed); 
  digitalWrite(columnPort[modulo((onPort-1),9)], LOW);

}
  delay(dt);
  
  
  digitalWrite(layerPort[modulo((onLayer-1),3)], LOW);
}
