#include "cubeled.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CUBELed w;
    w.show();

    return a.exec();
}
